# Reference: <https://postmarketos.org/vendorkernel>
# Kernel config based on: arch/arm64/configs/msm8939_defconfig

_flavor="postmarketos-qcom-msm8939"
pkgname=linux-$_flavor
pkgver=5.9_rc7_git20210713
pkgrel=1
pkgdesc="Mainline kernel fork for Qualcomm MSM8939 devices"
arch="aarch64"
_carch="arm64"
url="https://gitlab.com/msm8939-mainline/linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-nftables
	"
makedepends="bash bc bison devicepkg-dev installkernel flex openssl-dev perl"
provides="linux-huawei-kiwi=$pkgver-r$pkgrel" # for backwards compatibility
replaces="linux-huawei-kiwi"

_repository="linux"
_tag="5.9-rc7-v1"
_config="config-$_flavor.$arch"
source="
	$pkgname-$_tag.tar.gz::$url/-/archive/$_tag/linux-$_tag.tar.gz
	$_config
"
builddir="$srcdir/$_repository-$_tag"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$CARCH" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS"
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir"/usr/share/dtb
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}
sha512sums="
a50083ea1ef33b2326a4b241a82f923f7529155969523052dd3c3f330a7ff194f1ef081cb698fc1d866efca2d73ca027dbbc68edf265b88daebabde901a31613  linux-postmarketos-qcom-msm8939-5.9-rc7-v1.tar.gz
e397231c446d40cfc9e7054cf471203e8776ff0104f3d938c73bb854f9366c926cf4b6315939ee915b7a75dc82b95bcd81012c460e26e861cc5141f521116b66  config-postmarketos-qcom-msm8939.aarch64
"
